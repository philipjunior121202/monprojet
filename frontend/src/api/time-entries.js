import axios from 'axios'

const apiClient = axios.create({
  baseURL: 'http://127.0.0.1:8000/api/',
  headers: {
    'Content-Type': 'application/json'
  }
})

export default {
  getTimeEntries() {
    return apiClient.get('time-entries/')
  },
  createTimeEntry(timeEntry) {
    return apiClient.post('time-entries/', timeEntry)
  }
}