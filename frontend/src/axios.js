import axios from 'axios';

// Fonction pour récupérer le cookie CSRF
function getCookie(name) {
  let cookieValue = null;
  if (document.cookie && document.cookie !== '') {
    const cookies = document.cookie.split(';');
    for (let i = 0; i < cookies.length; i++) {
      const cookie = cookies[i].trim();
      if (cookie.substring(0, name.length + 1) === (name + '=')) {
        cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
        break;
      }
    }
  }
  return cookieValue;
}

//  instance Axios avec la configuration de base
const instance = axios.create({
  baseURL: 'http://127.0.0.1:8000/api/', // je peux utiliser le http ou https
  headers: {
    'Authorization': 'Bearer my-token', // il faut que je trouve mon jeton
    'Content-Type': 'application/json',
    'X-CSRFToken': getCookie('csrftoken')
  }
});

// Ajouter un intercepteur pour inclure le CSRF Token sur les requêtes nécessitant la protection CSRF
instance.interceptors.request.use(config => {
  // Récupérer et ajouter le CSRF Token au header 'X-CSRFTOKEN'
  const csrfToken = getCookie('csrftoken');
  if (csrfToken) {
    config.headers['X-CSRFTOKEN'] = csrfToken;
  }
  return config;
});

export default instance;
