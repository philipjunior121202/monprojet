import { createApp } from 'vue'
import App from './App.vue'
import axios from 'axios'

// Configuration d'Axios
axios.defaults.baseURL = 'http://127.0.0.1:8080/api/'

// Configuration des paramètres CSRF
axios.defaults.xsrfHeaderName = 'X-CSRFTOKEN';  // Nom de l'en-tête CSRF
axios.defaults.xsrfCookieName = 'csrftoken';    // Nom du cookie CSRF

// Créer l'application Vue
const app = createApp(App)

// Ajouter Axios à l'instance de l'application
app.config.globalProperties.$http = axios

// Monter l'application
app.mount('#app')
