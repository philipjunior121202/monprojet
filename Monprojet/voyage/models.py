from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User

class TimeEntry(models.Model):
    title = models.CharField(max_length=100)
    description = models.TextField(blank=True)
    start_time = models.DateTimeField()
    end_time = models.DateTimeField()
    duration = models.DurationField(editable=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, default=timezone.now)
    def save(self, *args, **kwargs):
        self.duration = self.end_time - self.start_time
        super().save(*args, **kwargs)




    class Meta:
        app_label = 'voyage'  # Définir l'étiquette de l'application

