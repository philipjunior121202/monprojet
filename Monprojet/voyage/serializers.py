from rest_framework import serializers
from .models import TimeEntry
from django.contrib.auth.models import User

class TimeEntrySerializer(serializers.ModelSerializer):
    user = serializers.PrimaryKeyRelatedField(queryset=User.objects.all())
    class Meta:
        model = TimeEntry
        fields = '__all__'
        #fields = ['id', 'title', 'description', 'start_time', 'end_time', 'duration', 'created_at', 'updated_at']
        



